## Extended ReflectionClass

![Downloads](https://img.shields.io/packagist/dt/nstwf/extended-reflection-class?style=flat-square)
![Pipeline status](https://img.shields.io/gitlab/pipeline-status/nstwf/extended-reflection-class?branch=main&style=flat-square)
![Code coverage](https://img.shields.io/gitlab/coverage/nstwf/extended-reflection-class/main?color=d&logo=fff&logoColor=aggsa&style=flat-square)
![Tag](https://img.shields.io/gitlab/v/tag/nstwf/extended-reflection-class?style=flat-square)
![PHP](https://img.shields.io/packagist/php-v/nstwf/extended-reflection-class?style=flat-square)

### Features:

- List of use statements ([supported formats](https://www.php.net/manual/en/language.namespaces.importing.php))

```php
$reflectionClass = new ExtendedReflectionClass(MyClass::class);

$useStatementsMap = $reflectionClass->getUseStatementsMap();

//    UseStatementMap Object
//    (
//        [useStatements:\UseStatementMap:private] => Array
//            (
//                [BarClass] => \UseStatement Object
//                    (
//                        [class:\UseStatement:private] => \Implementation\Nested\BarClass
//                        [alias:\UseStatement:private] => BarClassAlias
//                    )
//            )
//    
//        [useStatementAliases:\UseStatementMap:private] => Array
//            (
//                [BarClassAlias] => \UseStatement Object
//                    (
//                        [class:\UseStatement:private] => \Implementation\Nested\BarClass
//                        [alias:\UseStatement:private] => BarClassAlias
//                    )
//            )
//    
//    )
```