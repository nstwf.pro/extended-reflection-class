<?php


namespace Nstwf\ExtendedReflectionClass;


use Nstwf\ExtendedReflectionClass\Implementation\ConstUseObject;
use Nstwf\ExtendedReflectionClass\Implementation\FunctionUseObject;
use Nstwf\ExtendedReflectionClass\Implementation\GlobalUseObject;
use Nstwf\ExtendedReflectionClass\Implementation\MultipleAndGroupUseObject;
use Nstwf\ExtendedReflectionClass\Implementation\Parent\Child\ChildObject;
use Nstwf\ExtendedReflectionClass\Implementation\Parent\NoParentObject;
use Nstwf\ExtendedReflectionClass\UseStatement\UseStatement;
use Nstwf\ExtendedReflectionClass\UseStatement\UseStatementMap;
use PHPUnit\Framework\TestCase;


/**
 * @covers \Nstwf\ExtendedReflectionClass\ExtendedReflectionClass
 */
class ExtendedReflectionClassTest extends TestCase
{
    public function testMultipleAndGroupUse()
    {
        // Arrange
        $reflectionClass = new ExtendedReflectionClass(MultipleAndGroupUseObject::class);

        $expected = UseStatementMap::new()
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass', 'BazClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass', 'FooClassAlias'));

        // Act
        $actual = $reflectionClass->getUseStatementsMap();

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testFunctionUse()
    {
        // Arrange
        $reflectionClass = new ExtendedReflectionClass(FunctionUseObject::class);

        $expected = UseStatementMap::new()
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass'));

        // Act
        $actual = $reflectionClass->getUseStatementsMap();

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testGlobalUse()
    {
        // Arrange
        $reflectionClass = new ExtendedReflectionClass(GlobalUseObject::class);

        $expected = UseStatementMap::new()
            ->add(new UseStatement('ArrayObject'));

        // Act
        $actual = $reflectionClass->getUseStatementsMap();

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testConstUse()
    {
        // Arrange
        $reflectionClass = new ExtendedReflectionClass(ConstUseObject::class);

        $expected = UseStatementMap::new()
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass'));

        // Act
        $actual = $reflectionClass->getUseStatementsMap();

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testParentClass()
    {
        $reflectionClass = new ExtendedReflectionClass(ChildObject::class);

        $this->assertInstanceOf(ExtendedReflectionClass::class, $reflectionClass->getParentClass());
    }

    public function testNoParentClass()
    {
        $reflectionClass = new ExtendedReflectionClass(NoParentObject::class);

        $this->assertFalse($reflectionClass->getParentClass());
    }
}