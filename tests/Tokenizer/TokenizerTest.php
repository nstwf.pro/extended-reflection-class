<?php


namespace Nstwf\ExtendedReflectionClass\Tokenizer;


use Nstwf\ExtendedReflectionClass\Implementation\ConstUseObject;
use Nstwf\ExtendedReflectionClass\Implementation\GlobalUseObject;
use Nstwf\ExtendedReflectionClass\Tokenize\Tokenizer;
use Nstwf\ExtendedReflectionClass\UseStatement\UseStatement;
use Nstwf\ExtendedReflectionClass\UseStatement\UseStatementMap;
use PHPUnit\Framework\TestCase;


class TokenizerTest extends TestCase
{
    public function testSimpleUse()
    {
        // Arrange
        $reflectionClass = new Tokenizer();

        $expected = UseStatementMap::new()
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass'));

        // Act
        $actual = $reflectionClass->tokenize('
<?php

namespace Nstwf\ExtendedReflectionClass\Implementation;

use Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass;
        ');

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testAliasUse()
    {
        // Arrange
        $reflectionClass = new Tokenizer();

        $expected = UseStatementMap::new()
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass', 'BarClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass', 'BazClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass', 'FooBarClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass', 'FooClassAlias'));

        // Act
        $actual = $reflectionClass->tokenize('
<?php

namespace Nstwf\ExtendedReflectionClass\Implementation;

use Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass as BarClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass as BazClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass as FooBarClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass as FooClassAlias;
        ');

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testGroupSimpleUse()
    {
        // Arrange
        $reflectionClass = new Tokenizer();

        $expected = UseStatementMap::new()
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass', 'BarClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass', 'FooBarClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass'));

        // Act
        $actual = $reflectionClass->tokenize('
<?php

namespace Nstwf\ExtendedReflectionClass\Implementation;

use Nstwf\ExtendedReflectionClass\Implementation\Nested\{BarClass as BarClassAlias};
use Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass as FooBarClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass;
        ');

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testMultipleSimpleUse()
    {
        // Arrange
        $reflectionClass = new Tokenizer();

        $expected = UseStatementMap::new()
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass'));

        // Act
        $actual = $reflectionClass->tokenize('
<?php

namespace Nstwf\ExtendedReflectionClass\Implementation;

use Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass;
        ');

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testMultipleAliasUse()
    {
        // Arrange
        $reflectionClass = new Tokenizer();

        $expected = UseStatementMap::new()
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass', 'BarClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass', 'BazClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass', 'FooBarClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass', 'FooClassAlias'));

        // Act
        $actual = $reflectionClass->tokenize('
<?php

namespace Nstwf\ExtendedReflectionClass\Implementation;

use Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass as BarClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass as BazClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass as FooBarClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass as FooClassAlias;
        ');

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testGroupAliasUse()
    {
        // Arrange
        $reflectionClass = new Tokenizer();

        $expected = UseStatementMap::new()
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass', 'BarClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass', 'BazClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass', 'FooBarClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass', 'FooClassAlias'));

        // Act
        $actual = $reflectionClass->tokenize('
<?php

namespace Nstwf\ExtendedReflectionClass\Implementation;

use Nstwf\ExtendedReflectionClass\Implementation\Nested\{BarClass as BarClassAlias};
use Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass as BazClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass as FooBarClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass as FooClassAlias;
        ');

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testGroupSimpleAndAliasUse()
    {
        // Arrange
        $reflectionClass = new Tokenizer();

        $expected = UseStatementMap::new()
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass', 'BarClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass', 'FooBarClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass'));

        // Act
        $actual = $reflectionClass->tokenize('
<?php

namespace Nstwf\ExtendedReflectionClass\Implementation;

use Nstwf\ExtendedReflectionClass\Implementation\Nested\{BarClass as BarClassAlias};
use Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass as FooBarClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass;
        ');

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testMultipleSimpleAndAliasUse()
    {
        // Arrange
        $reflectionClass = new Tokenizer();

        $expected = UseStatementMap::new()
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass', 'BazClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass', 'FooClassAlias'));

        // Act
        $actual = $reflectionClass->tokenize('
<?php

namespace Nstwf\ExtendedReflectionClass\Implementation;

use Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass as BazClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass as FooClassAlias;
        ');

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testMultipleAndGroupUse()
    {
        // Arrange
        $reflectionClass = new Tokenizer();

        $expected = UseStatementMap::new()
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass', 'BazClassAlias'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass', 'FooClassAlias'));

        // Act
        $actual = $reflectionClass->tokenize('
<?php

namespace Nstwf\ExtendedReflectionClass\Implementation;

use Nstwf\ExtendedReflectionClass\Implementation\Nested\{BarClass};
use Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass as BazClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass as FooClassAlias;
');

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testFunctionUse()
    {
        // Arrange
        $reflectionClass = new Tokenizer();

        $expected = UseStatementMap::new()
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass'));

        // Act
        $actual = $reflectionClass->tokenize('
<?php

namespace Nstwf\ExtendedReflectionClass\Implementation;

use Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass;


use function explode;
        ');

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testGlobalUse()
    {
        // Arrange
        $reflectionClass = new Tokenizer();

        $expected = UseStatementMap::new()
            ->add(new UseStatement('ArrayObject'));

        // Act
        $actual = $reflectionClass->tokenize('
<?php

namespace Nstwf\ExtendedReflectionClass\Implementation;

use ArrayObject;
        ');

        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testConstUse()
    {
        // Arrange
        $reflectionClass = new Tokenizer();

        $expected = UseStatementMap::new()
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass'))
            ->add(new UseStatement('Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass'));

        // Act
        $actual = $reflectionClass->tokenize('
<?php

namespace Nstwf\ExtendedReflectionClass\Implementation;

use Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass;

use const PHP_INT_MAX;
        ');

        // Assert
        $this->assertEquals($expected, $actual);
    }
}