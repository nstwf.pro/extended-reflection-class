<?php


declare(strict_types=1);


namespace Nstwf\ExtendedReflectionClass\Implementation;


use ArrayObject;


final class GlobalUseObject
{
    private ArrayObject $arrayObject;
}