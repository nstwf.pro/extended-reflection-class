<?php


declare(strict_types=1);


namespace Nstwf\ExtendedReflectionClass\Implementation;


use Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass as BazClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass as FooClassAlias;


final class MultipleSimpleAndAliasUseObject
{
    private BarClass $bar;
    private FooClassAlias $foo;
    private BazClassAlias $baz;
    private FooBarClass $fooBar;
}