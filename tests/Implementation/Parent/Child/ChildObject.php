<?php


declare(strict_types=1);


namespace Nstwf\ExtendedReflectionClass\Implementation\Parent\Child;


use Nstwf\ExtendedReflectionClass\Implementation\Parent\ParentObject;


final class ChildObject extends ParentObject
{

}