<?php


declare(strict_types=1);


namespace Nstwf\ExtendedReflectionClass\UseStatement;


use ArrayIterator;
use IteratorAggregate;


final class UseStatementMap
{
    private array $useStatements = [];
    private array $useStatementAliases = [];

    public static function new(): self
    {
        return new self();
    }

    public function add(UseStatement $useStatement): self
    {
        $this->useStatements[$useStatement->getShortName()] = $useStatement;

        if ($useStatement->getAlias()) {
            $this->useStatementAliases[$useStatement->getAlias()] = $useStatement;
        }

        return $this;
    }

    public function get(string $classOrAlias): UseStatement
    {
        if (array_key_exists($classOrAlias, $this->useStatements)) {
            return $this->useStatements[$classOrAlias];
        }

        if (array_key_exists($classOrAlias, $this->useStatementAliases)) {
            return $this->useStatementAliases[$classOrAlias];
        }

        throw new \InvalidArgumentException("Not found class or alias $classOrAlias");
    }

    public function hasUse(string $classOrAlias): bool
    {
        return array_key_exists($classOrAlias, $this->useStatements) || array_key_exists($classOrAlias, $this->useStatementAliases);
    }
}