<?php


declare(strict_types=1);


namespace Nstwf\ExtendedReflectionClass\Tokenize;


use Nstwf\ExtendedReflectionClass\UseStatement\UseStatement;
use Nstwf\ExtendedReflectionClass\UseStatement\UseStatementMap;


final class Tokenizer
{
    public function tokenize(string $source): UseStatementMap
    {
        $tokens = token_get_all($source);

        $useStatementsMap = new UseStatementMap();

        $storedUse = false;
        $groupUse = false;
        $aliasUse = false;

        $useNamespace = '';
        $useAliasName = '';
        $currentUse = '';
        $groupClassName = '';

        foreach ($tokens as $token) {
            [$code, $value] = $token;

            if ($code === T_USE) {
                $storedUse = true;
            }

            if (in_array($code, [T_FUNCTION, T_CONST])) {
                $storedUse = false;
            }

            if ($storedUse) {
                if (in_array($token, [',', ';'])) {
                    if ($groupUse && $aliasUse) {
                        $useStatementsMap->add(new UseStatement($useNamespace . '\\' . $groupClassName, $useAliasName));
                    } elseif ($groupUse) {
                        $useStatementsMap->add(new UseStatement($useNamespace . '\\' . $groupClassName));
                    } elseif ($aliasUse) {
                        $useStatementsMap->add(new UseStatement($currentUse, $useAliasName));
                    } else {
                        $useStatementsMap->add(new UseStatement($currentUse));
                    }
                }

                if ($token === ';') {
                    $useNamespace = '';
                    $currentUse = '';
                    $groupUse = '';
                    $storedUse = false;
                    $aliasUse = false;
                }

                if ($token === '{') {
                    $groupUse = true;
                }

                if ($code === T_AS) {
                    $aliasUse = true;
                }

                if (in_array($code, [T_STRING, T_NS_SEPARATOR, T_NAME_QUALIFIED])) {
                    if ($groupUse && $aliasUse) {
                        $useAliasName = $value;
                    } elseif ($groupUse) {
                        $groupClassName = $value;
                    } elseif ($aliasUse) {
                        $useAliasName = $value;
                    } else {
                        $currentUse = $value;

                        if (!$useNamespace) {
                            $useNamespace = $value;
                        }
                    }
                }
            }
        }

        return $useStatementsMap;
    }
}