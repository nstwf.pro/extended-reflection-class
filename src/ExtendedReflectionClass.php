<?php


declare(strict_types=1);


namespace Nstwf\ExtendedReflectionClass;


use Nstwf\ExtendedReflectionClass\Reader\SourceReader;
use Nstwf\ExtendedReflectionClass\Tokenize\Tokenizer;
use Nstwf\ExtendedReflectionClass\UseStatement\UseStatementMap;
use ReflectionClass;


class ExtendedReflectionClass extends ReflectionClass
{
    protected UseStatementMap $useStatements;
    protected bool $useStatementsParsed = false;

    public function getParentClass(): self|false
    {
        if ($parentClass = parent::getParentClass()) {
            return new ExtendedReflectionClass($parentClass);
        }

        return false;
    }

    public function getUseStatementsMap(): UseStatementMap
    {
        return $this->parseUseStatements();
    }

    private function parseUseStatements(): UseStatementMap
    {
        if (!$this->useStatementsParsed) {
            $source = $this->readFileSource();

            $this->useStatements = $this->tokenize($source);
            $this->useStatementsParsed = true;
        }

        return $this->useStatements;
    }

    private function readFileSource(): string
    {
        $reader = new SourceReader();

        return $reader->read($this->getFileName(), $this->getStartLine());
    }

    private function tokenize(string $source): UseStatementMap
    {
        $tokenizer = new Tokenizer();

        return $tokenizer->tokenize($source);
    }
}